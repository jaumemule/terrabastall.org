<?php

namespace Controllers\User;

use Model\User\UserModel;
use Controllers\Mailer\IMailer;
use Controllers\Mailer\MailFactory;
use Controllers\System\RandomString;
use Controllers\User\PasswordHash;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class Register
 * @package Controllers\User
 */
class Register
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app, Request $request, UserModel $UserModel, IMailer $mailer, MailFactory $mailFactory)
    {
        $this->req         = $request;
        $this->UserModel   = $UserModel;
        $this->mailer      = $mailer;
        $this->mailFactory = $mailFactory;
        $this->app         = $app;
    }

    /**
     * @return Response
     */
    public function index()
    {
        if(!@$this->req->request->get("email") ||
            !@$this->req->request->get("pass") ||
            !@$this->req->request->get("cpass") ||
            !@$this->req->request->get("name") ||
            !@$this->req->request->get("lastname") ||
            !@$this->req->request->get("lastname2") ||
            !@$this->req->request->get("address") ||
            !@$this->req->request->get("tel") ||
            !@$this->req->request->get("date") ||
            !@$this->req->request->get("dni") ||
            !@$this->req->request->get("bank")
        ){
            return new JsonResponse(["status"  => "fail",
                                     "message" => "Falten camps per omplir"
                                    ]);
        }


        if($this->UserModel->ifUserExist(filter_var($this->req->request->get("email"), FILTER_SANITIZE_EMAIL))){

            return new JsonResponse(["status"  => "fail",
                                     "message" => "Aquest compte ja existeix..."
                                    ]);      
        }

        $this->num_soci = $this->UserModel->countUsers() + 1;
        $this->user_data = [
            'email'             => filter_var($this->req->request->get("email"), FILTER_SANITIZE_EMAIL),
            'name'              => filter_var($this->req->request->get("name"), FILTER_SANITIZE_STRING),
            'surname1'          => filter_var($this->req->request->get("lastname"), FILTER_SANITIZE_STRING),
            'surname2'          => filter_var($this->req->request->get("lastname2"), FILTER_SANITIZE_STRING),
            'address'           => filter_var($this->req->request->get("address"), FILTER_SANITIZE_STRING),
            'telf'              => filter_var($this->req->request->get("tel"), FILTER_SANITIZE_STRING),
            'birth'             => filter_var($this->req->request->get("date"), FILTER_SANITIZE_STRING),
            'dni'               => filter_var($this->req->request->get("dni"), FILTER_SANITIZE_STRING),
            'adulthood'         => 1,
            'CP'                => 0,
            'number_associated' => $this->num_soci,
            'date'              => date("Y-m-d H:i:s"),
            'bank'              => filter_var($this->req->request->get("bank"), FILTER_SANITIZE_STRING),
        ];

        $registering = $this->UserModel->register($this->user_data);

        if($registering){

            $Hash       = new \Controllers\System\RandomString(30);
            $randomhash = $Hash->randomHash();

            $Password = new \Controllers\User\PasswordHash($this->req->request->get("pass"));
            $generated_password = $Password->hashPassword();

            $this->user_account = [
                'email'             => filter_var($this->req->request->get("email"), FILTER_SANITIZE_EMAIL),
                'password'          => $generated_password,
                'username'          => strtolower( filter_var($this->req->request->get("name"), FILTER_SANITIZE_STRING).filter_var($this->req->request->get("lastname"), FILTER_SANITIZE_STRING).filter_var($this->req->request->get("lastname2"), FILTER_SANITIZE_STRING) ),
                'hash'              => $randomhash,
                'register_id'       => $registering,
            ];

            $creating_account = $this->UserModel->createAccount($this->user_account);

            if($creating_account){

                //fer aquí un try catch d'envio de email de confirmació
                try {
                    $this->sendRegisterEmail($this->user_data['email'], $this->user_data['name'], $randomhash);
                } catch (\SendGrid\Exception $e) {
                    return new JsonResponse(["status"  => "error",
                                             "message" => "Email could not been send. Error: " . $e->getCode()
                    ]);
                }
                return new JsonResponse(['status' => "success", 'data' => '']);

            }

            return new JsonResponse(["status"  => "fail",
                                     "message" => "No s'ha pogut crear el compte d'usuari"
                                    ]);
        }

        return new JsonResponse(["status"  => "fail",
                                 "message" => "No s'han pogut insertar les dades"
                                ]);
    }

    private function sendRegisterEmail($email, $firstname, $hash)
    {
        $mail = $this->mailFactory->create($email, 'Confirmació del registre', $this->buildBody($firstname, $hash));
        $this->mailer->sendEmail($mail);
    }

    private function buildBody($name, $hash)
    {

        return $this->app['twig']->render('mailing/register.twig', [
           "name"                      => $name,
           "numero_soci"               => $this->num_soci,
           "confirmation_token"        => $hash,
        ]);
    }

}
