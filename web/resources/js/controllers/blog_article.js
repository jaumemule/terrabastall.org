    $(document).ready(function() {

        $( "#publish" ).click(function(event) {
            event.preventDefault();
            publishBlog(1);
        });  
        $( "#unpublish" ).click(function(event) {
            event.preventDefault();
            publishBlog(0);
        });    



        $( "#subscriuBtn" ).click(function() {

            apiCall('/subscribe', {
                email: $("#email_subscribe").val() ,
                lang: Lang ,

            }, 'POST', 'APP',
                function (result) {

                    if (result.status == "success") {

                        alert("Subscrit correctament!");

                        $('#subscriu_f')[0].reset();
                        $("#subscriu_m").show().delay(5000).fadeOut();
                        //$("#msform").find('fieldset').hide().fadeOut();
                    } else {
                        alert("Un error ha sorgit");
                    }
                    console.log(result);
                },
                function (xhr, errorType, error) {
                    console.log(xhr, errorType, error)
                }
            );
        });  


    });

    function publishBlog( status ){

        apiCall('/blog/publish/' + $("#url").val(), { status: status }, 'PUT', 'APP', function (result) {
                if (result.status == "success") {
                      location.reload();
                    //$("#msform").find('fieldset').hide().fadeOut();
                } else {
                     alert("Ha sorgit un problema: " + result.message);
                }
            },
            function (xhr, errorType, error) {
                console.log(xhr, errorType, error)
            }, "", {"csrfToken": $("#csrfToken").val()}

        );
    }

