<?php

namespace Controllers\User;

use Model\User\UserModel;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Controllers\User\PasswordHash;

/**
 * Class Login
 * @package Controllers\User
 */
class Login
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app, Request $request, UserModel $UserModel)
    {
        $this->app         = $app;
        $this->req         = $request;
        $this->UserModel   = $UserModel;
    }

    /**
     * @return Response
     */
    public function index()
    {
        if(!@$this->req->request->get("user") ||
            !@$this->req->request->get("pass")
        ){
            return new JsonResponse(["status"  => "fail",
                                     "message" => "Falten camps per omplir"
                                   ]);
        }

        $Password = new \Controllers\User\PasswordHash( filter_var($this->req->request->get("pass"), FILTER_SANITIZE_STRING) );
        $generated_password = $Password->hashPassword();

        $login = $this->UserModel->iflogin(

                            filter_var($this->req->request->get("user"), FILTER_SANITIZE_EMAIL) ,
                            $generated_password

                        );

        if($login){

            $this->app['session']->set('uid', $login);

            return new JsonResponse(["status"  => "success",
                                     "message" => "Loguejat correctament"
                                    ]);      
        }

       /* $this->num_soci = $this->UserModel->countUsers() + 1;
        $this->user_data = [
            'email'             => filter_var($this->req->request->get("email"), FILTER_SANITIZE_EMAIL),
            'pass'              => filter_var($this->req->request->get("name"), FILTER_SANITIZE_STRING),
        ];

        $registering = $this->UserModel->register($this->user_data);*/


        return new JsonResponse(["status"  => "fail",
                                 "message" => "Usuari o contrasenya incorrectes"
                                ]);
    }

}
