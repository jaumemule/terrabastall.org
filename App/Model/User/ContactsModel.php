<?php

namespace Model\User;

class ContactsModel
{
    private $app;
    
    function __construct($app)
    {
        $this->app = $app;
    }

    public function insertEmail( $data){
       $this->app['db']->insert('contacts', $data);
       return $this->app['db']->lastInsertId();
    }

    public function insertEmailtoList($data){

        return $this->app['db']->insert('contacts_has_lists', $data);

    }

    public function insertList($data){

        return $this->app['db']->insert('lists', $data);

    }


    public function countContacts()
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('u.id')
            ->from('contacts', 'u');

        $queryBuilder->groupBy('u.email');

        return count($rows = $this->app['db']->fetchAll($queryBuilder));

    }
    /*
select l.*, (select count(chl.contacts_id) from contacts_has_lists chl where chl.lists_id = l.id) as total 
from lists l 
inner join contacts_has_lists chl on chl.lists_id = l.id 
inner join  contacts c on chl.contacts_id 
group by l.name;

SELECT l.*, (count(chl.contacts_id) from contacts_has_lists chl where chl.lists_id = l.id) as total FROM lists l INNER JOIN contacts_has_lists h ON l.id = h.lists_id LEFT JOIN contacts c ON c.id = h.contacts_id WHERE c.newsletter = 1
*/
    public function getListsWithPeople( )
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('l.*, (select count(chl.contacts_id) from contacts_has_lists chl where chl.lists_id = l.id) as total')
            ->from('lists', 'l')
            ->innerJoin('l', 'contacts_has_lists', 'h', 'l.id = h.lists_id')
            ->leftJoin('h', 'contacts', 'c', 'c.id = h.contacts_id');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("c.newsletter", 1 ));
        $filters->add($queryBuilder->expr()->eq("l.active", 1 ));

        $queryBuilder->where($filters);
        $queryBuilder->groupBy('l.id'); //important per no repetir llistes

        return $this->app['db']->fetchAll($queryBuilder);
    }

//SELECT date, count(id) from contacts group by YEAR(date), MONTH(date), DAY(date) limit 0,100;
    public function getStatisticsFromContacts( )
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('c.date as Periode, count(c.id) as Contactes')
            ->from('contacts', 'c');

        $queryBuilder->groupBy('YEAR(c.date), MONTH(c.date), DAY(c.date)'); 

        return $this->app['db']->fetchAll($queryBuilder);
    }


    public function getStatisticsFromSocis( )
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('c.date as Periode, count(c.id) as Socis')
            ->from('register', 'c');

        $queryBuilder->groupBy('YEAR(c.date), MONTH(c.date), DAY(c.date)'); 

        return $this->app['db']->fetchAll($queryBuilder);
    }


    public function getAllLists( )
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

       /* $queryBuilder->select('l.*')
            ->from('lists', 'l');*/

        $queryBuilder->select('l.*, (select count(chl.contacts_id) from contacts_has_lists chl where chl.lists_id = l.id) as total')
            ->from('lists', 'l')
            ->leftJoin('l', 'contacts_has_lists', 'h', 'l.id = h.lists_id')
            ->leftJoin('h', 'contacts', 'c', 'c.id = h.contacts_id');
        $queryBuilder->groupBy('l.id'); //important per no repetir llistes

        return $this->app['db']->fetchAll($queryBuilder);
    }

    public function ifContactExist($email)
    {

        $emailf = $email['email'];
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('r.id')
            ->from('contacts', 'r');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("r.email", "'".$emailf."'" ));
        $queryBuilder->where($filters);

        count($rows = $this->app['db']->fetchAll($queryBuilder)) ? $result = $rows[0]['id'] : $result = false;

        return $result;

    }


    public function getContacts( $list, $active = 1, $spam_filter = 'filter' )
    {


    /*  select c.* from contacts c 
        inner join contacts_has_lists h 
        on c.id = h.contacts_id 
        inner join lists l 
        on l.id = h.lists_id 
        where l.name = 'Socis' 
        and c.newsletter = 1 group by c.email;

    */   

        //PATCH
        $break_lists = explode(',', $list);
        $final_list = array();

        for ($i=0; $i < count($break_lists); $i++) { 
            array_push($final_list, $break_lists[$i]);
        }

        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('c.*')
            ->from('contacts', 'c')
            ->innerJoin('c', 'contacts_has_lists', 'h', 'c.id = h.contacts_id')
            ->innerJoin('h', 'lists', 'l', 'l.id = h.lists_id');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->in("l.id", $final_list ));
        $filters->add($queryBuilder->expr()->eq("c.newsletter", $active ));

        //     PATCH:    OMETEM HOTMAIL I OUTLOOK PER SPAM

        if($spam_filter == 'filter'){
            $filters->add($queryBuilder->expr()->notlike("c.email", $queryBuilder->expr()->literal('%hotmail%') ));
        }

        if($spam_filter == 'reverse'){
            $filters->add($queryBuilder->expr()->like("c.email", $queryBuilder->expr()->literal('%hotmail%') ));
        }

        $queryBuilder->where($filters);
        $queryBuilder->groupBy('c.email'); //important per no repetir emails en diferentes llistes

        return $this->app['db']->fetchAll($queryBuilder);

    } 
}
