<?php

namespace Controllers\Socis;

use Symfony\Component\HttpFoundation\Response;
use Model\User\UserModel;

class SocisHome
{

    const NO_CONFIRMATS             = 0;
    const CONFIRMATS_PERO_NO_PAGATS = 1;
    private $app;

    /**
     * @param $app
     */
    public function __construct($app, UserModel $UserModel)
    {
        $this->app = $app;
        $this->UserModel = $UserModel;
    }

    /**
     * @return Response
     */
    public function index()
    {

        $registres = $this->UserModel->getSocisRegistersInfo();

        foreach ($registres as $key => $value) {
            $registres[$key]['status'] = 'warning';
            if(@$value['quota']){
                $registres[$key]['status'] = 'success';
            }

            if(@$value['active'] == self::NO_CONFIRMATS){
                $registres[$key]['status'] = 'danger';
            }
        }

        $response = new Response($this->app['twig']->render('socis.twig', [
            "registres"             => $registres,
            "no_confirmats"         => $this->UserModel->getSocisRegistersNoConfirmats( self::NO_CONFIRMATS ),
            "confirmats_no_pagats"  => $this->UserModel->getSocisRegistersNoConfirmats( self::CONFIRMATS_PERO_NO_PAGATS ),
        ]));

        return $response;
    }
}
