<?php

namespace Handlers\LoginHandler;

/**
 * Class LoginHandler
 * @package Handlers\LoginHandler
 */
final class LoginHandler
{
    /**
     * @param $handler
     *
     * @return mixed
     */
    public static function take($handler)
    {
        /*
        if(!$app['session']){
            return false;
        }
        */

        return $_SESSION;
    }
}
