<?php

namespace Controllers\Contacts;
use Controllers\Mailer\IMailer;
use Controllers\Mailer\MailFactory;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Model\User\ContactsModel;
use Model\User\UserModel;
use Controllers\Contacts\Contacts;

class Emailer
{
    /**
     * @var \Silex\Application
     */
    private $app;
    const TESTING_EMAIL  = 2;
    const FINAL_EMAILING = 1;
    const NO_ACTIU       = 0;
    const ACTIVE         = 1;
    const SPAM_FILTER    = 'filter'; //true: nomes els no spam, false: tots els emails, reverse: nomes els spam
    const SPAM_EMAILS    = 'reverse'; //true: nomes els no spam, false: tots els emails, reverse: nomes els spam

    public function __construct($app, Request $request, ContactsModel $ContactsModel, IMailer $mailer, MailFactory $mailFactory, Contacts $Contacts)
    {
        $this->app              = $app;
        $this->req              = $request;
        $this->ContactsModel    = $ContactsModel;
        $this->mailer           = $mailer;
        $this->mailFactory      = $mailFactory;
        $this->Contacts         = $Contacts;
    }

    /**
     * @return Response
     */
    public function getSendTemplate( )
    {

        return $this->app['twig']->render('emailer.twig', [
            'lists'         => $this->ContactsModel->getListsWithPeople() ,
            "csrfToken"     =>  $this->app['session']->get('csrf_token') ? $this->app['session']->get('csrf_token') : null
        ]);

    }    

    public function getListsTemplate( )
    {

        $lists = $this->ContactsModel->getAllLists();

        foreach ($lists as $key => $value) {
            $lists[$key]['status'] = '';
            $lists[$key]['active'] = 'Sí';

            if(@$value['active'] == self::NO_ACTIU){

                $lists[$key]['active'] = 'No';
                $lists[$key]['status'] = 'danger';
            }
        }

        return $this->app['twig']->render('lists.twig', [
            'lists'         => $lists ,
            "csrfToken"     =>  $this->app['session']->get('csrf_token') ? $this->app['session']->get('csrf_token') : null
        ]);

    }   

    public function getAddTemplate( )
    {

        return $this->app['twig']->render('addcontacts.twig', [
            'lists'         => $this->ContactsModel->getAllLists() ,
            "csrfToken"     =>  $this->app['session']->get('csrf_token') ? $this->app['session']->get('csrf_token') : null
        ]);

    }   


    public function addContacts( $listID )
    {
        if(@$listID){


            $broken_emails = explode(',', $this->req->request->get("emails"));

            for ($i=0; $i < count($broken_emails); $i++) {  
                $this->Contacts->insertEmailInList($broken_emails[$i], $listID, 'cat');
            }

            return new JsonResponse(['status' => "success", 'data' => 'Emails afegits correctament']);

        }

        return new JsonResponse(["status"  => "fail",
                 "message" => "No has seleccionat cap llista"
                ]);  
    } 

    public function send()
    {

        if(@$this->req->request->get("titol") 
            && @$this->req->request->get("flag") 
            && @$this->req->request->get("motiu") 
            && @$this->req->request->get("missatge")
            ){

            if($this->req->request->get("flag") == self::TESTING_EMAIL){
                $response = $this->sendTestingEmail();
            }

            if($this->req->request->get("flag") == self::FINAL_EMAILING){
                $response = $this->sendMassiveEmails();
            }

            return new JsonResponse(['status' => "success", 'data' => $response]);
        }

        return new JsonResponse(["status"  => "fail",
                         "message" => "Falten camps obligatoris per omplir"
                        ]);     

    }


    public function addList( )
    {
     $response = $this->ContactsModel->insertList( 
            [
                'name'          =>    filter_var($this->req->request->get("name"), FILTER_SANITIZE_STRING) ,
                'concept'       =>    filter_var($this->req->request->get("assumpte")) , 
                'active'        =>    1 ,
                'date'          =>    date("Y-m-d")
            ]
        );

        return new JsonResponse(['status' => "success", 'data' => $response]);

    }

    public function getSpamContacts()
    {
        $contacts = $this->ContactsModel->getContacts(
            $this->req->request->get("lists"), 
            self::ACTIVE,
            self::SPAM_EMAILS
        );  

        return new JsonResponse(['status' => "success", 'data' => $contacts]);

    }

    private function sendMassiveEmails()
    {
        
        $lists = $this->ContactsModel->getContacts(
            $this->req->request->get("lists"), 
            self::ACTIVE,
            self::SPAM_FILTER
        );
        
        foreach ($lists as $key => $value) {

            $this->sendFinalEmail($value['email'], 
                $this->req->request->get("motiu"),
                $this->req->request->get("missatge"),
                $this->req->request->get("titol")
            );
        }
        
    }

    private function sendTestingEmail()
    {
                                    //FER SERVIR EL MAIL DUSUARI
        return $this->sendFinalEmail('ahmterrabastall@gmail.com', 
            $this->req->request->get("motiu"),
            $this->req->request->get("missatge"),
            $this->req->request->get("titol")
        );
    }

    private function sendFinalEmail($emailDestinatari, $motiu, $missatge, $titol)
    {
        $mail = $this->mailFactory->create($emailDestinatari, $motiu, $this->buildBody( $titol, $missatge ));

        $this->mailer->sendEmail($mail);
    }

    private function buildBody($titol, $missatge)
    {
        return $this->app['twig']->render('mailing/default.twig', [
            'titol'         => $titol,
            'missatge'     => $missatge
        ]);
    }


}
