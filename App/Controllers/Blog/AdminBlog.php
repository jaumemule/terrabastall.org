<?php

namespace Controllers\Blog;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Model\Blog\BlogModel;

class AdminBlog
{
    /**
     * @var \Silex\Application
     */
    private $app;
    private $BlogModel;

    public function __construct($app, BlogModel $BlogModel)
    {
        $this->app = $app;
        $this->BlogModel = $BlogModel;
    }

    /**
     * @return Response
     */
    public function index( )
    {
        
        $response = new Response($this->app['twig']->render('adminbloglist.twig', [
                'data'      =>  $this->BlogModel->getAllArticles( )
        ]));

        return $response;
    }    

    public function createTpl( )
    {
        
        $response = new Response($this->app['twig']->render('adminblogcreate.twig', [

        ]));

        return $response;
    } 

    public function edit( $url )
    {
        $article = $this->BlogModel->getArticle($url);
        
        if($article){

            $response = new Response($this->app['twig']->render('adminblogedit.twig', [

                'data'      => $article[0]

            ]));

            return $response;
        }

        return false;

    } 

    public function post( )
    {

       $blogID = $this->BlogModel->insertArticle( 
            [
                'content'       =>   $this->app['request']->request->get('text') ,
                'title'         =>   $this->app['request']->request->get('title') ,
                'abstract'      =>   $this->app['request']->request->get('abstract') ,
                'url'           =>   $this->sanitizeURL( $this->app['request']->request->get('title') ) ,
                'date'          =>   date("Y-m-d H:i:s") ,
                'uid'           =>   $this->app['session']->get('uid')
            ]
        );

       if($blogID){
            return new JsonResponse(['status' => "success", 'data' => $this->BlogModel->getArticleURL( $blogID )[0]]);
       }
        
        return new JsonResponse(['status' => "fail", 'data' => 'no s\'ha pogut insertar una nova entrada al blog']);
    } 

    public function putData( $url )
    {
        $newURL = $this->sanitizeURL( $this->app['request']->request->get('title') );

       $blogID = $this->BlogModel->putArticle( 
            [
                'content'       =>   $this->app['request']->request->get('text') ,
                'title'         =>   $this->app['request']->request->get('title') ,
                'abstract'      =>   $this->app['request']->request->get('abstract') ,
                'url'           =>   $newURL ,
                'date'          =>   date("Y-m-d H:i:s") ,
                'uid'           =>   $this->app['session']->get('uid')
            ] ,
            [ 'url' => $url ]
        );

        return new JsonResponse(['status' => "success", 'data' => $newURL ]);
       
    }    

    public function publish( $url )
    {

       $this->BlogModel->putArticle( 
            [
                'active'       =>   $this->app['request']->request->get('status') ,
            ] ,
            [ 'url' => $url ]
        );

        return new JsonResponse(['status' => "success", 'data' => 'Canvis realitzats en lestat de la publicació' ]);
       
    }    

    /**
     * Function: sanitize
     * Returns a sanitized string, typically for URLs.
     *
     * Parameters:
     *     $string - The string to sanitize.
     *     $force_lowercase - Force the string to lowercase?
     *     $anal - If set to *true*, will remove all non-alphanumeric characters.
     */
    private function sanitizeURL($string, $force_lowercase = true, $anal = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                       "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                       "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }
}
