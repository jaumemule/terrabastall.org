<?php

use Model\RedisSingleton;
use Symfony\Component\Translation\Loader\YamlFileLoader;

// front controller
$app = new \Silex\Application();

// ################## Oficial silex providers register ######################
// Twig provider for templating
// twig.path is /views and autoescape ought to be on
$app->register(new \Silex\Provider\TwigServiceProvider(), [
    'twig.path' => __DIR__ . '/../views',
]);

$app['twig']->addGlobal('pathToResources', '/../resources');

//CREATE USER 'terrabastall'@'localhost' IDENTIFIED BY 'Terrabastall666';
// GRANT UPDATE ON terrabastall.* TO terrabastall@localhost;

// // session provider to handle user sessions
// $app->register(new \Silex\Provider\SessionServiceProvider());
// $app->register(new \Silex\Provider\DoctrineServiceProvider(), [
//     'dbs.options' => [
//         'mysql_read'  => [
//             'driver'   => 'pdo_mysql',
//             'host'     => 'localhost',
//             'dbname'   => 'terrabastall_db',
//             'user'     => 'terrabastall_usr',
//             'password' => 't3rr4b4st4ll',
//             'charset'  => 'utf8',
//         ],
//         'mysql_write' => [
//             'driver'   => 'pdo_mysql',
//             'host'     => 'localhost',
//             'dbname'   => 'terrabastall_db',
//             'user'     => 'terrabastall_usr',
//             'password' => 't3rr4b4st4ll',
//             'charset'  => 'utf8',
//         ],
//     ],
// ]);

// session provider to handle user sessions
$app->register(new \Silex\Provider\SessionServiceProvider());
$app->register(new \Silex\Provider\DoctrineServiceProvider(), [
    'dbs.options' => [
        'mysql_read'  => [
            'driver'   => 'pdo_mysql',
            'host'     => 'localhost',
            'dbname'   => 'terrabastall_db',
            'user'     => 'dbuser',
            'password' => '123',
            'charset'  => 'utf8',
        ],
        'mysql_write' => [
            'driver'   => 'pdo_mysql',
            'host'     => 'localhost',
            'dbname'   => 'terrabastall_db',
            'user'     => 'dbuser',
            'password' => '123',
            'charset'  => 'utf8',
        ],
    ],
]);


//CHANGING LANGUAGE
if (@$_GET['lang']) {
    $app['session']->set('lang', $_GET['lang']);
}

$app->register(new Silex\Provider\TranslationServiceProvider(), [
    'locale_fallbacks'  => [ 'cat' ],
    'locale'            => $app['session']->get('lang') ,
]);

//######################### Services register ##########################
$app['rest'] = $app->share(function () use ($app) {
    return new Rest($app['auth.string']);
});
/*
$app['redis'] = function () {
    return RedisSingleton::getInstance();
};
*/


$app['translator'] = $app->share($app->extend('translator', function ($translator, $app) {
    $translator->addLoader('yaml', new YamlFileLoader());
   // $translator->addResource('yaml', __DIR__ . '/Translations/' . $app['session']->get('lang') . '.yml', $app['session']->get('lang'));
    $translator->addResource('yaml', __DIR__.'/Translations/en.yml', 'en');
    $translator->addResource('yaml', __DIR__.'/Translations/cat.yml', 'cat');
    $translator->addResource('yaml', __DIR__.'/Translations/es.yml', 'es');
    return $translator;
}));

$app['mailer'] = function () {
    return new \Controllers\Mailer\SendGridMailer();
};

$app['mail-factory'] = function () {
    return new \Controllers\Mailer\MailFactory();
};


/* ****** MODELS ******** */

$app['UserModel']        = function ($app) {
    return new \Model\User\UserModel($app);
};

$app['ContactsModel']        = function ($app) {
    return new \Model\User\ContactsModel($app);
};

$app['BlogModel']        = function ($app) {
    return new \Model\Blog\BlogModel($app);
};

/* ****** BODY CALLS ******** */

$app['home'] = function ($app) {
    return new \Controllers\Home\HomeController($app, $app["UserModel"]);
};

$app['blog'] = function ($app) {
    return new \Controllers\Home\Blog($app, $app['BlogModel'], $app["UserModel"]);
};

$app['public_venues'] = function ($app) {
    return new \Controllers\Venues\Venues($app);
};

$app['login'] = function ($app) {
    return new \Controllers\Login\LoginController($app, $app["UserModel"]);
};

$app['logout'] = function ($app) {
    return new \Controllers\User\Logout($app);
};

$app['adminblog'] = function ($app) {
    return new \Controllers\Blog\AdminBlog($app, $app['BlogModel']);
};

$app['dologin'] = function ($app) {
    return new \Controllers\User\Login($app, $app["request"], $app["UserModel"]);
};

$app['dashboard'] = function ($app) {
    return new \Controllers\Dashboard\DashboardHome($app, $app["UserModel"], $app['contacts']);
};

$app['socis'] = function ($app) {
    return new \Controllers\Socis\SocisHome($app, $app["UserModel"]);
};

$app['emailer'] = function ($app) {
    return new \Controllers\Contacts\Emailer($app, $app["request"], $app["ContactsModel"], $app["mailer"], $app['mail-factory'], $app['contacts']);
};

/* ****** LOGICAL CONTROLLERS ******* */

$app['subscribe'] = function ($app) {
    return new \Controllers\Contacts\Subscriptions($app, $app["request"], $app["contacts"]);
};

$app['contacts'] = function ($app) {
    return new \Controllers\Contacts\Contacts($app, $app["ContactsModel"]);
};

/* ****** EVENT LISTENERS ******** */
$app['register'] = function ($app) {
    return new \Controllers\User\Register($app, $app["request"], $app["UserModel"], $app["mailer"], $app['mail-factory']);
};

$app['managelists'] = function ($app) {
    return new \Controllers\Contacts\Lists($app, $app["request"], $app["ContactsModel"]);
};

$app['account'] = function ($app) {
    return new \Controllers\User\Account(
        $app, $app["request"], $app["UserModel"], $app["mailer"], $app['mail-factory'], $app['contacts']
    );
};
