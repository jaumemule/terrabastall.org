    $(document).ready(function() {

        // validate signup form on keyup and submit
        $( "#submitform" ).click(function() {

            var email       = $("#email").val();
            var pass        = $("#pass").val();
            var cpass       = $("#cpass").val();
            var name        = $("#name").val();
            var lastname    = $("#lastname").val();
            var lastname2   = $("#lastname2").val();
            var address     = $("#address").val();
            var tel         = $("#tel").val();
            var date        = $("#date").val();
            var bank        = $("#bank").val();
            var dni        = $("#dni").val();

            if(pass != cpass){
                    
                $('#errorForm').html("Les contrasenyes no coincideixen");
                $("#errorForm").show().delay(5000).fadeOut();

            }else{


            if(email == ""
                    || pass == ""
                    || cpass == ""
                    || name == ""
                    || lastname == ""
                    || lastname2 == ""
                    || address == ""
                    || tel == ""
                    || date == ""
                    || bank == ""
                    || dni == ""
            ){
                $("#errorForm").html("Error: Els camps del formulari no son correctes o falten camps per omplir. Si el problema persisteix contacta'ns a info@terrabastall.org");
                $("#errorForm").show().delay(5000).fadeOut();
             }else{

                apiCall('/register', {
                    email: email ,
                    pass: pass ,
                    cpass: cpass ,
                    name: name ,
                    lastname: lastname ,
                    lastname2: lastname2 ,
                    address: address ,
                    tel: tel ,
                    date: date ,
                    dni: dni ,
                    bank: bank ,
                    lang: Lang
                }, 'POST', 'APP',
                    function (result) {

                        if (result.status == "success") {
                            $('#msform')[0].reset();
                            $("#successForm").show().delay(10000).fadeOut();
                            //$("#msform").find('fieldset').hide().fadeOut();
                        } else {
                            $('#errorForm').html("Un error ha sorgit. Torna-ho a intentar i, si persisteix, sisplau: contacta'ns a info@terrabastall.org");
                            $("#errorForm").show().delay(10000).fadeOut();
                        }
                        console.log(result);
                    },
                    function (xhr, errorType, error) {
                        console.log(xhr, errorType, error)
                    }
                );

            }
             
            }
        });    


        $( "#subscriuBtn" ).click(function() {

            apiCall('/subscribe', {
                email: $("#email_subscribe").val() ,
                lang: Lang ,

            }, 'POST', 'APP',
                function (result) {

                    if (result.status == "success") {
                        $('#subscriu_f')[0].reset();
                        $("#subscriu_m").show().delay(5000).fadeOut();
                        //$("#msform").find('fieldset').hide().fadeOut();
                    } else {
                        alert("Un error ha sorgit");
                    }
                    console.log(result);
                },
                function (xhr, errorType, error) {
                    console.log(xhr, errorType, error)
                }
            );
        });    
    });