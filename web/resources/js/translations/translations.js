translate = {

   es: function( key ){

      return this.data_es[key];

    } ,

   en: function( key ){

      return this.data_en[key];

    } ,

    data_en: {

      'none_selected'   : 'None selected' ,
      'select_all'      : ' Select all' ,
      'multiselect_all' : 'multiselect-all' ,
      'search'          : 'Search' ,
      'selected'        : 'selected' ,
      'all_selected'    : 'All selected',
      'i_like_it'       : 'I like it',
      'i_dont_like_it'  : 'I don\'t like it anymore',
      'fb_login'        : 'Logging with Facebook...',
      'gplus_login'     : 'Logging with Gmail...',
      'request_sent'    : 'Request has been sent',
      'accepted'        : 'Accepted',
      'deleted'         : 'Deleted'

    },

    data_es: {
	    'none_selected'   : 'No hay selecciones' ,
      'select_all'      : 'Seleccionar todos',
      'multiselect_all' : 'Multiseleccionar todos',
      'search'          : 'Buscar',
      'selected'        : 'Seleccionado',
      'i_like_it'       : 'Me gusta',
      'i_dont_like_it'  : 'Ya no me gusta',
      'fb_login'        : 'Iniciando con Facebook...',
      'gplus_login'     : 'Iniciando con Gmail...',
      'request_sent'    : 'Solicitud enviada',
      'accepted'        : 'Aceptado',
      'deleted'         : 'Eliminado'
    }

};
