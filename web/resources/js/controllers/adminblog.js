CKEDITOR.replace( 'editor' );

    $(document).ready(function() {

        $( "#sendfinal" ).click(function(event) {
            event.preventDefault();
            createBlog();
        });    
    });

    function createBlog( ){

        var text = CKEDITOR.instances.editor.getData();
        var title = $("#title").val();
        var abstract = $("#abstract").val();

        //flag = 1 = definitiu / flag = 2 test
        apiCall('/adminblog/create', { abstract: abstract, title: title, text: text  }, 'POST', 'APP', function (result) {
            console.log(result.data);
                if (result.status == "success") {
                      window.location = '/blog/' + result.data.url;
                    //$("#msform").find('fieldset').hide().fadeOut();
                } else {
                     alert("Ha sorgit un problema: " + result.message);
                }
            },
            function (xhr, errorType, error) {
                console.log(xhr, errorType, error)
            }, "", {"csrfToken": $("#csrfToken").val()}

        );
    }
