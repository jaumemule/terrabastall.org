<?php
namespace Controllers\Contacts;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Controllers\Contacts\Contacts;

class Subscriptions
{
    /**
     * @var \Silex\Application
     */
    private $app;
    const   SUBSCRIPTION_TERRABASTALL_LIST = 24;

    public function __construct($app, Request $request, Contacts $Contacts)
    {
        $this->app = $app;
        $this->req = $request;
        $this->Contacts = $Contacts;
    }

    public function index( )
    {

        if( $this->Contacts->insertEmailInList( 
                
                    filter_var($this->req->request->get("email"), FILTER_SANITIZE_EMAIL),
                    self::SUBSCRIPTION_TERRABASTALL_LIST ,
                    $this->req->request->get("lang")
                
            )
        ){

            return new JsonResponse(['status' => "success", 'data' => 'Subscrit correctament']);

        }
        return new JsonResponse(["status"  => "fail",
                                 "message" => "No s'han pogut insertar les dades"
                                ]);
    }

}
