<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once(BASE_DIR . '/App/Rest.php');
require_once(BASE_DIR . '/App/config.php');
require_once(BASE_DIR . '/App/Bootstrap.php');
//require_once(BASE_DIR . '/App/Handlers/LoginHandler.php');

$app = require(BASE_DIR . '/App/routing.php');
/**
 * This method runs before any action defined in the
 * src/controllers.php
 *
 * In this case handles authentication using "HTTP Basic Authentication"
 * If you have not received your username and password correctly
 * Returns the error code 403 - Unauthorized. In case of not wanting to control
 * Service using a username and password could comment on the content
 */

function generateToken()
{
    return md5(uniqid(rand(), true)); //PROVISIONAL
}

$app->before(function (Request $request) use ($app) {

    $app["request"]->getSession()->start(); //NO ESTIC SEGUR
    $sessionId = $app['session']->getId();

    //se comprueba si existe token de sesión
    if (!$app['session']->has('csrf_token')) {
        $app['session']->set('csrf_token', generateToken());
    }

    //CHECK DEFAULT LANGUAGE
    if (@!$app['session']->has('lang')) {
        $app['session']->set('lang', DEFAULT_LANGUAGE);
    }

});

/**
 * This method manages the place errors will not
 * Managed and controlled by us as a general catch.
 */
$app->error(function (\Exception $e, $code) use ($app) {

    if ($app['debug']) {
        return;
    }
    switch ($code) {
        case 404:
            $message = 'The page you are looking for does not exist.';
            break;
        default:
            $message = 'An error has occurred while processing your request.';
            break;
    }

    return new Response($message, $code);
});

/**
* Definim la ruta relativa als resources
*/
$app['asset_path']          = '/resources';
$app['domain']              = 'terrabastall.org';
$app['general_email']       = 'info@terrabastall.org';
//$app['asset_path']  = '/resources';
$app['lang']        = $app['session']->get('lang');


return $app;
