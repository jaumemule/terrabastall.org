<?php

namespace Controllers\User;

use Model\User\UserModel;
use Controllers\Mailer\IMailer;
use Controllers\Mailer\MailFactory;
use Controllers\System\RandomString;
use Controllers\Home\HomeController;
use Controllers\Contacts\Contacts;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class Register
 * @package Controllers\User
 */
class Account
{
    /**
     * @var \Silex\Application
     */
    private $app;
    const LLISTA_SOCIS = 1;
    /**
     * @param $app
     */
    public function __construct($app, Request $request, UserModel $UserModel, IMailer $mailer, MailFactory $mailFactory, Contacts $Contacts)
    {
        $this->req         = $request;
        $this->UserModel   = $UserModel;
        $this->mailer      = $mailer;
        $this->mailFactory = $mailFactory;
        $this->Contacts    = $Contacts;
        $this->app         = $app;
    }

    public function confirmAccount()
    {

        $Home = new \Controllers\Home\HomeController($this->app, $this->UserModel);

        if(@$this->req->query->get("hash")){

            $email = $this->UserModel->checkRegisterHash($this->req->query->get("hash"));

            if($email){


                $activate_account = $this->UserModel->activateRegister($email['email']);

                $this->sendConfirmationEmail($email['email']);
                $this->Contacts->insertEmailInList($email['email'], self::LLISTA_SOCIS);

                return $Home->index('Compte activat correctament!');

            }

            return $Home->index('El compte ja ha estat validat o el registre ha caducat :( escriu-nos a info@terrabastall.org');

        }

        return $Home->index('La validació és incorrecta');
 
    }

    private function sendConfirmationEmail($emailDestinatari)
    {
        $mail = $this->mailFactory->create($emailDestinatari, 'Usuari activat', $this->buildBody());

        $this->mailer->sendEmail($mail);
    }

    private function buildBody()
    {
        return $this->app['twig']->render('mailing/account.twig', []);
    }

}
