<?php

namespace Controllers\Dashboard;

use Symfony\Component\HttpFoundation\Response;
use Model\User\UserModel;
use Controllers\Contacts\Contacts;

class DashboardHome
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app, UserModel $UserModel, Contacts $Contacts)
    {
        $this->app = $app;
        $this->UserModel = $UserModel;
        $this->Contacts = $Contacts;
    }

    /**
     * @return Response
     */
    public function index( $main_message = false )
    {
        
        $response = new Response($this->app['twig']->render('dashboard.twig', [
            "total_socis"       => $this->UserModel->countUsers(),
            "total_contacts"    => $this->Contacts->countContacts(),
        ]));

        return $response;
    }
}
