<?php

namespace Controllers\Home;

use Model\Names\NameFilterModel;
use Symfony\Component\HttpFoundation\Response;
use Model\User\UserModel;

/**
 * Class HomeController
 * @package Controllers\Home
 */
class HomeController
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app, UserModel $UserModel)
    {
        $this->app = $app;
        $this->UserModel = $UserModel;
    }

    /**
     * @return Response
     */
    public function index( $main_message = false )
    {
        
        $response = new Response($this->app['twig']->render('home.twig', [
            "total_socis"    => $this->UserModel->countUsers(),
            "missatge"       => $main_message,
        ]));

        return $response;
    }
    /**
     * @return Response
     */
    public function home_test( $main_message = false )
    {
        
        $response = new Response($this->app['twig']->render('home_test.twig', [
            "total_socis"    => $this->UserModel->countUsers(),
            "missatge"       => $main_message,
        ]));

        return $response;
    }
}
