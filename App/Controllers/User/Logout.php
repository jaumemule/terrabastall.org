<?php

namespace Controllers\User;

use Model\User\UserModel;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class Logout
 * @package Controllers\User
 */
class Logout
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app)
    {
        $this->app         = $app;
    }

    /**
     * @return Response
     */
    public function index()
    {
        
        $this->app['session']->remove('uid');
        return new JsonResponse(["status"  => "success",
                                 "message" => "Desloguejat"
                                ]);
    }

}
