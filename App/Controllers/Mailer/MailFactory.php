<?php

namespace Controllers\Mailer;

/**
 * Class MailFactory
 * @package Controllers\Mailer
 */
class MailFactory
{
    /**
     * @param $address
     * @param $subject
     * @param $body
     *
     * @return Mail
     */
    public function create($address, $subject, $body)
    {
        return new Mail($subject, $body, $address);
    }
}
