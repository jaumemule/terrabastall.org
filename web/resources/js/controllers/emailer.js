CKEDITOR.replace( 'editor' );

    $(document).ready(function() {

        $( "#sendfinal" ).click(function() {

            lists = new Array();

            lists = $('input[name="lists_check"]:checked');
            var total_lists = 'empty';

            for (var i = lists.length - 1; i >= 0; i--) {

                if(total_lists == 'empty'){
                total_lists = $(lists[i]).val();

                }else{

                total_lists = total_lists + ',' + $(lists[i]).val();
                }
            };
            console.log(total_lists);
        sendEmails( 1, total_lists );
        });    

        $( "#sendtest" ).click(function() {
            sendEmails( 2, 0 );
        });    

        $( ".lists_check" ).click(function() {

            prelists = new Array();

            prelists = $('input[name="lists_check"]:checked');
            var pre_total_lists = 'empty';

            for (var i = prelists.length - 1; i >= 0; i--) {

                if(pre_total_lists == 'empty'){
                pre_total_lists = $(prelists[i]).val();

                }else{

                pre_total_lists = pre_total_lists + ',' + $(prelists[i]).val();
                }
            };

            getSpamEmails(pre_total_lists);
        });    


    });

    function getSpamEmails( id ){

 
        //flag = 1 = definitiu / flag = 2 test
        apiCall('/emailer/emails/spam', { lists: id }, 'POST', 'APP', function (result) {

                if (result.status == "success") {
                     $("#spam").html('');
                     $("#spam").append('<b>No s\'enviaran per evitar Spam:</b><br> ');

                     for (var i = result.data.length - 1; i >= 0; i--) {
                        $("#spam").append(result.data[i].email + ', ');
                     };

                    //$("#msform").find('fieldset').hide().fadeOut();
                } else {
                     alert("Ha sorgit un problema: " + result.message);
                }
            },
            function (xhr, errorType, error) {
                console.log(xhr, errorType, error)
            }, "", {"csrfToken": $("#csrfToken").val()}

        );
    }

    function sendEmails( flag, lists ){

        var missatge = CKEDITOR.instances.editor.getData();
        var titol = $("#titol").val();
        var motiu = $("#assumpte").val();
  
        //flag = 1 = definitiu / flag = 2 test
        apiCall('/emailer/send', { flag: flag, motiu: motiu, titol: titol, missatge: missatge, lists: lists }, 'POST', 'APP', function (result) {

                if (result.status == "success") {
                     alert("Missatges enviats");
                    //$("#msform").find('fieldset').hide().fadeOut();
                } else {
                     alert("Ha sorgit un problema: " + result.message);
                }
            },
            function (xhr, errorType, error) {
                console.log(xhr, errorType, error)
            }, "", {"csrfToken": $("#csrfToken").val()}

        );
    }
