<?php

namespace Controllers\Blog;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminBlog
{
    /**
     * @var \Silex\Application
     */
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @return Response
     */
    public function index( )
    {
        
        $response = new Response($this->app['twig']->render('adminbloglist.twig', [

        ]));

        return $response;
    }    

    public function createTpl( )
    {
        
        $response = new Response($this->app['twig']->render('adminblogcreate.twig', [

        ]));

        return $response;
    }    

}
