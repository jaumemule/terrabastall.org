CKEDITOR.replace( 'editor' );

    $(document).ready(function() {

        $( "#editfinal" ).click(function(event) {
            event.preventDefault();
            editBlog();
        });    
    });

    function editBlog( ){

        var text = CKEDITOR.instances.editor.getData();
        var title = $("#title").val();
        var abstract = $("#abstract").val();

        //flag = 1 = definitiu / flag = 2 test
        apiCall('/blog/edit/' + $("#url").val(), { abstract: abstract, title: title, text: text  }, 'PUT', 'APP', function (result) {
            console.log(result.data);
                if (result.status == "success") {
                      window.location = '/blog/' + result.data;
                    //$("#msform").find('fieldset').hide().fadeOut();
                } else {
                     alert("Ha sorgit un problema: " + result.message);
                }
            },
            function (xhr, errorType, error) {
                console.log(xhr, errorType, error)
            }, "", {"csrfToken": $("#csrfToken").val()}

        );
    }
