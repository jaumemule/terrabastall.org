<?php

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/* ** MIDDLEWARES ** */

$session = function (Request $request, Application $app) {
    $csfrTokenHeader = $request->headers->get('csrfToken');
    $sessionToken    = $app['session']->get('csrf_token');
    $uid             = $app['session']->get('uid');

    //if ($csfrTokenHeader != $sessionToken && $sessionToken) {
    if (!$uid) {
        return \Symfony\Component\HttpFoundation\JsonResponse::create([
            'status' => "error",
            'data'   => "Inicia sessió per accedir a aquesta secció"
        ]);
        die();
    }
};

/* ******** BODY CONTROLLERS *******a* */

$app->get('/', function () use ($app) {
    return $app['home']->index();
});

$app->get('/home_test', function () use ($app) {
    return $app['home']->home_test();
});

$app->get('login', function () use ($app) {
    return $app['login']->index();
});

$app->get('blog', function () use ($app) {
    return $app['blog']->index();
});

$app->get('blog/{url}', function ( $url ) use ($app) {
    return $app['blog']->article( $url );
});

//gestio

/* *** BLOG *** */
$app->get('adminblog', function () use ($app) {
    return $app['adminblog']->index();
})->before($session);

$app->post('adminblog/create', function () use ($app) {
    return $app['adminblog']->post();
})->before($session);

$app->get('adminblog/create', function () use ($app) {
    return $app['adminblog']->createTpl();
})->before($session);

$app->get('adminblog/edit/{url}', function ($url) use ($app) {
    return $app['adminblog']->edit($url);
})->before($session);

$app->put('blog/edit/{url}', function ($url) use ($app) {
    return $app['adminblog']->putData($url);
})->before($session);

$app->put('blog/publish/{url}', function ($url) use ($app) {
    return $app['adminblog']->publish($url);
})->before($session);

/* *** FI BLOG *** */

/* *** PAGINA DE EVENTOS *** */


$app->get('venues', function () use ($app) {
    return $app['public_venues']->listAll($url);
});

/* *** FI PAGINA DE EVENTOS *** */


/* *** INTRANET *** */

$app->post('logout', function () use ($app) {
    return $app['logout']->index();
})->before($session);

$app->get('dashboard', function () use ($app) {
    return $app['dashboard']->index();
})->before($session);

$app->get('socis', function () use ($app) {
    return $app['socis']->index();
})->before($session);

$app->get('emailer', function () use ($app) {
    return $app['emailer']->getSendTemplate();
})->before($session);

$app->get('addcontacts', function () use ($app) {
    return $app['emailer']->getAddTemplate();
})->before($session);

$app->get('lists', function () use ($app) {
    return $app['emailer']->getListsTemplate();
})->before($session);

/* ******** LISTENERS CONTROLLERS ******* */

$app->post('register', function () use ($app) {
    return $app['register']->index();
});

$app->post('login', function () use ($app) {
    return $app['dologin']->index();
});


$app->get('confirmregister', function () use ($app) {
    return $app['account']->confirmAccount();
});


$app->post('emailer/send', function () use ($app) {
    return $app['emailer']->send();
})->before($session);


$app->post('emailer/emails/spam', function () use ($app) {
    return $app['emailer']->getSpamContacts();
})->before($session);


$app->post('emailer/add/{listID}', function ( $listID ) use ($app) {
    return $app['emailer']->addContacts( $listID );
})->before($session);


$app->post('managelists', function ( ) use ($app) {
    return $app['emailer']->addList( );
})->before($session);


$app->post('subscribe', function ( ) use ($app) {
    return $app['subscribe']->index( );
});


$app->get('statistics/contacts', function ( ) use ($app) {
    return $app['contacts']->statisticsContacts( );
})->before($session);


$app->get('statistics/socis', function ( ) use ($app) {
    return $app['contacts']->statisticsSocis( );
})->before($session);

/*

//POST (también tenemos GET) para obtener listado de nombres despues de login. Así se indican los likes
$app->post('names', function () use ($app) {
    return $app['names']->index();
});

//GET (tambien tenemos POST) para obtener listado nombres antes del login
$app->get('names', function () use ($app) {
    return $app['names']->index();
});

$app->get('nombre', function () use ($app) {
    return $app['nombre']->index();
});

$app->get('template', function () use ($app) {
    return $app['template']->index();
});

$app->get('perfil', function (Request $request) use ($app) {
    return $app['perfil']->index();
});

//SEO: preferible - a _
$app->get('nombres-encontrados', function (Request $request) use ($app) {
    return $app['nombres-encontrados']->index();
});

$app->get('buscar-amigos', function (Request $request) use ($app) {
    return $app['buscar-amigos']->index();
});

$app->get('sugerencias-nombres', function (Request $request) use ($app) {
    return $app['sugerencias-nombres']->index();
});

$app->post('delete-relationship', function (Request $request) use ($app) {
    return $app['delete-relationship']->index();
});
*/
/* ******** AJAX CONTROLLERS ******* */
/*
$ajaxMiddleware = function (Request $request, Application $app) {
    $csfrTokenHeader = $request->headers->get('csrfToken');
    $sessionToken    = $app['session']->get('csrf_token');

    if ($csfrTokenHeader != $sessionToken && $sessionToken) {
        return \Symfony\Component\HttpFoundation\JsonResponse::create([
            'status' => "error",
            'data'   => "Session token not valid"
        ]);
        die();
    }
};

$app->get('names_list', function (Request $request) use ($app) {
    return $app['names_list']->index($request);
})->before($ajaxMiddleware);

$app->post('login_ajax', function (Request $request) use ($app) {
    return $app['login_ajax']->index($request);
});//No ejecutar middleware ya que la pagina de login no cuelga de layout y no tiene el csfr token dispoible en el input hidden para poder enviarlo

$app->delete('user/{userId}', function ($userId) use ($app) {
    return $app['deleteuser']->index($userId);
})->before($ajaxMiddleware);

$app->get('login_ajax', function (Request $request) use ($app) {
    return $app['login_ajax']->index($request);
});//No ejecutar middleware ya que la pagina de login no cuelga de layout y no tiene el csfr token dispoible en el input hidden para poder enviarlo

$app->post('logout', function (Request $request) use ($app) {
    return $app['logout']->index();
})->before($ajaxMiddleware);

$app->post('likes', function () use ($app) {
    return $app['likename']->index();
})->before($ajaxMiddleware);

$app->delete('likes/{nameId}', function ($nameId) use ($app) {
    return $app['dislikename']->index($nameId);
})->before($ajaxMiddleware);

$app->get('buscar-amigos-searcher-ajax', function () use ($app) {
    return $app['buscar-amigos-searcher-ajax']->index();
})->before($ajaxMiddleware);

$app->get('buscar-amigos-scroll-ajax', function () use ($app) {
    return $app['buscar-amigos-scroll-ajax']->index();
})->before($ajaxMiddleware);

$app->post('relationship', function () use ($app) {
    return $app['add-relationship']->index();
})->before($ajaxMiddleware);

$app->delete('relationship/{relationshipId}', function ($relationshipId) use ($app) {
    return $app['delete-relationship']->index($relationshipId);
})->before($ajaxMiddleware);

$app->put('relationship/{relationshipId}', function ($relationshipId) use ($app) {
    return $app['accept-relationship']->index($relationshipId);
})->before($ajaxMiddleware);

$app->post('email-invitation', function (Request $request) use ($app) {
    return $app['add-email-invitation']->index();
})->before($ajaxMiddleware);

$app->get('nombres-encontrados-scroll-ajax', function (Request $request) use ($app) {
    return $app['nombres-encontrados-scroll-ajax']->index();
})->before($ajaxMiddleware);
*/
return $app;
