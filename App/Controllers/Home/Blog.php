<?php

namespace Controllers\Home;

use Model\Names\NameFilterModel;
use Symfony\Component\HttpFoundation\Response;
use Model\User\UserModel;
use Model\Blog\BlogModel;

/**
 * Class HomeController
 * @package Controllers\Home
 */
class Blog
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app, BlogModel $BlogModel, UserModel $UserModel)
    {
        $this->app       = $app;
        $this->BlogModel = $BlogModel;
        $this->UserModel = $UserModel;
    }

    /**
     * @return Response
     */
    public function index( $main_message = false )
    {
        
        $article = $this->BlogModel->getAllArticles();

        $response = new Response($this->app['twig']->render('blog.twig', [
            'data'      => $article
        ]));

        return $response;
    }


    public function article( $url )
    {

        $article = $this->BlogModel->getArticle($url);
        if($article){

            //PATCH GUARRO PER MIGRAR A USER MODEL
            if(!$article[0]['avatar']){
                $article[0]['avatar'] = 'default_avatar.jpg';
            }
                $session = true;

            if(!@$this->app['session']->get('uid')){
                $session = false;
            }

            if($article[0]['active'] || $session){

                $response = new Response($this->app['twig']->render('blog_article.twig', [
                    'session'   => $session,
                    'data'      => $article[0]
                ]));

                 return $response;

            }

            //AQUI el 404

            return false;
        }

        //RETORNAR PAGINA 404
        return false;
    }    

}
