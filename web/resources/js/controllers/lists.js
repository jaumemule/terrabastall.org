    $(document).ready(function() {

        $( "#addlist" ).click(function() {

            addlist( );
        });    

    });

    function addlist( ){

        var name        = $("#name").val();
        var assumpte    = $("#assumpte").val();
  
        apiCall('/managelists', { name: name, assumpte: assumpte }, 'POST', 'APP', function (result) {

                if (result.status == "success") {
                     location.reload();
                     $("#name").val("");
                } else {
                     alert("Ha sorgit un problema: " + result.message);
                }
            },
            function (xhr, errorType, error) {
                console.log(xhr, errorType, error)
            }, "", {"csrfToken": $("#csrfToken").val()}

        );
    }
