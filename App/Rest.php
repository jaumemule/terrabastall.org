<?php

/**
 * Class Rest
 */
class Rest
{
    /**
     * @var resource
     */
    protected $ch;

    /**
     * @param null $authString
     */
    public function __construct($authString = null)
    {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);

        if ($authString) {
            curl_setopt($this->ch, CURLOPT_USERPWD, $authString);
        }

        return $this;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    protected function execute()
    {
        $data = curl_exec($this->ch);
        $code = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        if ($code == 200) {
            return $data;
        } else {
            throw new \Exception('Code returned by the REST process: ' . $code);
        }
    }

    /**
     * @param $url
     *
     * @return $this
     */
    public function url($url)
    {
        curl_setopt($this->ch, CURLOPT_URL, $url);

        return $this;
    }

    /**
     * @param array $params
     * @param array $headers
     *
     * @return mixed
     * @throws Exception
     */
    public function post(array $params = [], array $headers =[])
    {
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        try {
            return $this->execute($this->ch);
        } catch (Exception $e) {
            throw new \Exception($e);
        }
    }

    /**
     * @param array $headers
     *
     * @return mixed
     * @throws Exception
     */
    public function get(array $headers = [])
    {
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        return $this->execute($this->ch);
    }


    public function delete(array $headers)
    {
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch,CURLOPT_HTTPHEADER, $headers);


        try {
            return $this->execute($this->ch);
        }catch (Exception $e){
            throw new \Exception($e);
        }

    }

    public function put(array $headers = [])
    {
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

        try {
            return $this->execute($this->ch);
        }catch (Exception $e){
            throw new \Exception($e);
        }
    }

}

