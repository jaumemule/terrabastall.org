<?php

namespace Controllers\Ticketing;

use Model\Names\NameFilterModel;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TicketingController
 * @package Controllers\Ticketing
 */
class Ticketing
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @return Response
     */
    public function premiumguest()
    {

        $response = new Response($this->app['twig']->render('ticketing_iframe.twig', [

        ]));

        return $response;
    }
}
