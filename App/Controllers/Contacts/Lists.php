<?php
namespace Controllers\Contacts;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Model\User\ContactsModel;

class Lists
{
    /**
     * @var \Silex\Application
     */
    private $app;

    public function __construct($app, Request $request, ContactsModel $ContactsModel)
    {
        $this->app = $app;
        $this->req = $request;
        $this->ContactsModel = $ContactsModel;
    }

    public function addList( )
    {
       $this->ContactsModel->insertList( 
            [
                'name'          =>    filter_var($this->req->request->get("name"), FILTER_SANITIZE_STRING) ,
                'concept'       =>    filter_var($this->req->request->get("assumpte") , 
                'active'        =>    1 ,
                'date'          =>    date("Y-m-d")
            ]
        );
    }

}
