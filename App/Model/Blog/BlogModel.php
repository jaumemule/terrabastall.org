<?php

namespace Model\Blog;

class BlogModel
{
    private $app;
    
    function __construct($app)
    {
        $this->app = $app;
    }

    public function insertArticle($data){
       $this->app['db']->insert('blog', $data);
       return $this->app['db']->lastInsertId();
    }

    public function putArticle($data, $refer){
       $this->app['db']->update('blog', $data, $refer);
    }

    // select * from blog where url = 'testejant-larticle'
    public function getArticleURL( $id )
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('b.url')
            ->from('blog', 'b');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("b.id", $id));

        $queryBuilder->where($filters);
        
        return $this->app['db']->fetchAll($queryBuilder);
    }
    // select * from blog where url = 'testejant-larticle'
    public function getArticle( $url )
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('b.*, u.avatar, r.name, r.surname1')
            ->from('blog', 'b')
            ->innerJoin('b', 'users', 'u', 'u.id = b.uid')
            ->innerJoin('u', 'register', 'r', 'r.id = u.register_id');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("b.url", "'" . $url . "'"));

        $queryBuilder->where($filters);
        
        return $this->app['db']->fetchAll($queryBuilder);
    }

    public function getAllArticles( )
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('b.*, u.avatar, r.name, r.surname1')
            ->from('blog', 'b')
            ->innerJoin('b', 'users', 'u', 'u.id = b.uid')
            ->innerJoin('u', 'register', 'r', 'r.id = u.register_id')
            ->orderBy('b.id', 'DESC');

        $data = $this->app['db']->fetchAll($queryBuilder);

        foreach ($data as $key => $value) {

            $data[$key]['active_name'] = 'No';
            $data[$key]['css_row'] = 'danger';

            if($value['active'] == 1){
                $data[$key]['active_name']  = 'Sí';
                $data[$key]['css_row']      = 'success';
            }
        }

        return $data;
    }
}
