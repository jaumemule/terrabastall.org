    $(document).ready(function() {

        $( "#logout" ).click(function() {

            logout( );
        });    

    });

    function logout( ){
        //flag = 1 = definitiu / flag = 2 test
        apiCall('/logout', { }, 'POST', 'APP', function (result) {

                if (result.status == "success") {
                     window.location = '/login';
                } else {
                     alert("Ha sorgit un problema: " + result.message);
                }
            },
            function (xhr, errorType, error) {
                console.log(xhr, errorType, error)
            }, "", {"csrfToken": $("#csrfToken").val()}

        );
    }
