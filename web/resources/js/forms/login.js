    $(document).ready(function() {

        // validate signup form on keyup and submit
        $( "#loginForm" ).submit(function(event) {
            event.preventDefault();
            var user       = $("#user").val();
            var pass        = $("#pass").val();

            if(user == ""
                    || pass == ""
            ){
                $("#errorForm").html("Error: Els camps del formulari no son correctes o falten camps per omplir. Si el problema persisteix contacta'ns a info@terrabastall.org");
                $("#errorForm").show().delay(5000).fadeOut();
             }else{

                apiCall('/login', {
                    user: user ,
                    pass: pass ,
                    lang: Lang
                }, 'POST', 'APP',
                    function (result) {

                        if (result.status == "success") {
                            window.location = '/dashboard';
                            //$("#msform").find('fieldset').hide().fadeOut();
                        } else {
                            $('#errorForm').html("Un error ha sorgit. Torna-ho a intentar i, si persisteix, sisplau: contacta'ns a info@terrabastall.org");
                            $("#errorForm").show().delay(10000).fadeOut();
                        }
                    },
                    function (xhr, errorType, error) {
                        console.log(xhr, errorType, error)
                    }
                );

            }
             
            
        });      
    });