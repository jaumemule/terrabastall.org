<?php
ini_set('display_errors', 0);
error_reporting(E_ALL);


require_once __DIR__ . '/../vendor/autoload.php';

define('BASE_DIR', realpath(__DIR__ . '/..'));
$app = require_once(BASE_DIR . '/App/app.php');

$app['debug'] = true;

$app['auth.user'] = AUTH_USER;
$app['auth.pass'] = AUTH_PASS;

$app['auth.string'] = $app['auth.user'] . ':' . $app['auth.pass'];

//--------------- ENVIRONMENT --------------------
//$app['rest.host'] = 'http://apibebes.dev/';
//$app['rest.host'] 	= 'http://api-bebes.noip.me/';

$app->run();
?>