<?php

namespace Controllers\Contacts;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Model\User\ContactsModel;

class Contacts
{
    /**
     * @var \Silex\Application
     */
    private $app;

    public function __construct($app, ContactsModel $ContactsModel)
    {
        $this->app = $app;
        $this->ContactsModel = $ContactsModel;
    }

    /**
     * @return Response
     */
    public function index( )
    {
        
    }    

    public function countContacts()
    {
       return $this->ContactsModel->countContacts();  
    }

    public function statisticsContacts()
    {
        return new JsonResponse(['status' => "success", 'data' => $this->ContactsModel->getStatisticsFromContacts()]);
    }

    public function statisticsSocis()
    {
        return new JsonResponse(['status' => "success", 'data' => $this->ContactsModel->getStatisticsFromSocis()]);
    }

    public function insertEmailInList( $email, $listID, $lang = 'cat' )
    {
        $contactID = $this->ifContactExist( $email, $lang );

       return $this->ContactsModel->insertEmailtoList( 
            [
                'contacts_id'     =>    $contactID,
                'lists_id'        =>    $listID 
            ]
        );
    }

    private function ifContactExist( $email, $lang )
    {
       $contactID = $this->ContactsModel->ifContactExist( 
            [
                'email'     =>   filter_var($email, FILTER_SANITIZE_EMAIL)
            ]
        );

       if($contactID){
            return $contactID;
       }

       return $this->ContactsModel->insertEmail( [ 'email' => filter_var($email, FILTER_SANITIZE_EMAIL), 'lang' => $lang, 'date' => date("Y-m-d H:i:s") ] );
    }

}
