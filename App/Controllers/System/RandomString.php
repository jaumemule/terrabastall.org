<?php

namespace Controllers\System;
    

class RandomString
{
    const HASH_STRING_TOKEN_CREATE = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
 
    public function __construct($len)
    {
        $this->len = $len;
    }

    public function randomHash()
    {
        $characters       = self::HASH_STRING_TOKEN_CREATE;
        $charactersLength = strlen($characters);
        $randomString     = '';
        for ($i = 0; $i < $this->len; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }
}
