    $(document).ready(function() {

        $( "#addEmails" ).click(function() {

            addEmails( );
        });    

    });

    function addEmails( ){

        var emails  = $("#emails").val();
        var list    = $("#list").val();
  
        //flag = 1 = definitiu / flag = 2 test
        apiCall('/emailer/add/' + list, { emails: emails }, 'POST', 'APP', function (result) {

                if (result.status == "success") {
                     alert("Correus afegits");
                     $("#emails").val("");
                } else {
                     alert("Ha sorgit un problema: " + result.message);
                }
            },
            function (xhr, errorType, error) {
                console.log(xhr, errorType, error)
            }, "", {"csrfToken": $("#csrfToken").val()}

        );
    }
