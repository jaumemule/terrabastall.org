<?php

namespace Model\User;

class UserModel
{
    private $app;
    
    function __construct($app)
    {
        $this->app = $app;
    }

    //Inerta un nou registre de soci.
    public function register($register)
    {
       $this->app['db']->insert('register', $register);
       return $this->app['db']->lastInsertId();
    }
    //Inerta un nou compte d'usuari.
    public function createAccount($users)
    {
       return $this->app['db']->insert('users', $users);
    }

    public function checkRegisterHash($hash)
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('u.email')
            ->from('users', 'u')
            ->leftJoin('u', 'register', 'r', 'r.id = u.register_id');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("r.active", 0));
        $filters->add($queryBuilder->expr()->eq("u.hash", '"' . $hash . '"' ));

        $queryBuilder->where($filters);

        count($rows = $this->app['db']->fetchAll($queryBuilder)) ? $result = $rows[0] : $result = false;

        return $result;

    }    

    //UNDER CONSTRUCTION
    public function getUserInfo()
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('u.username')
            ->from('users', 'u');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("u.id", 18 ));

        $queryBuilder->where($filters);

        return $this->app['db']->fetchAll($queryBuilder);

    }

    public function ifUserExist($email)
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('r.email')
            ->from('register', 'r');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("r.email", '"' . $email . '"' ));
        $queryBuilder->where($filters);

        count($rows = $this->app['db']->fetchAll($queryBuilder)) ? $result = true : $result = false;

        return $result;

    }

    public function iflogin($user, $password)
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('u.email, u.password, u.id')
            ->from('users', 'u');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("u.email", '"' . $user . '"' ));
        $filters->add($queryBuilder->expr()->eq("u.password", '"' . $password . '"' ));
        $queryBuilder->where($filters);

        count($rows = $this->app['db']->fetchAll($queryBuilder)) ? $result = $rows[0]['id'] : $result = false;

        return $result;

    }

    public function countUsers()
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('u.id')
            ->from('users', 'u');

        return count($rows = $this->app['db']->fetchAll($queryBuilder));

    }

    public function activateRegister($email)
    {
        $this->app['db']->update('register', ['active' => '1'], ['email' => $email]);
    }


    public function getSocisRegistersInfo()
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('r.*, p.date as renovat, p.quantity as quota')
            ->from('register', 'r')
            ->leftJoin('r', 'paid', 'p', 'r.id = p.register_id');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("p.concept", '"Quota"' ));
        $queryBuilder->where($filters);

        return $this->app['db']->fetchAll($queryBuilder);

    }

    public function getSocisRegistersNoConfirmats( $actiu = 1 )
    {
        $queryBuilder = $this->app['db']->createQueryBuilder();

        $queryBuilder->select('r.*')
            ->from('register', 'r');

        $filters = $queryBuilder->expr()->andx();
        $filters->add($queryBuilder->expr()->eq("r.active", $actiu ));
        $queryBuilder->where($filters);

        return $this->app['db']->fetchAll($queryBuilder);

    }

}
