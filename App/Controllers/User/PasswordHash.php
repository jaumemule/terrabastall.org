<?php

namespace Controllers\User;

final Class PasswordHash
{

	const SALT = 'ASa4045fs%&sljdfh3847';
	public function __construct($password){
		$this->password = $password;
	}

	public function hashPassword() {
		if($this->password != false) {
			$this->password = md5(md5(self::SALT).md5($this->password));
			return $this->password;
		}
		return false;
	}

}