<?php

namespace Controllers\Venues;

use Model\Names\NameFilterModel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Model\User\UserModel;
use Model\Blog\BlogModel;
use Curl\Curl;
use DateTime;
/**
 * Class Venues
 * @package Controllers\Venues
 */
class Venues
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app)
    {
        $this->app       = $app;
        $this->curl      = new Curl();
    }

    /**
     * @return Response
     */
    public function listAll( )
    {
        
        $venues = $this->curl->post('http://venuesmw.terrabastall.org:3003/proxy/venues', array(
            'target'    => '4whatparty,terrabastall,salagarage,innermetalproductions,forvictorybooking',
            'options'   => 'limit=15&debug=all',
            'fields'    => 'name,cover,category,description,place,updated_time,start_time,interested_count,attending_count,end_time',
            'orderBy'   => 'start_time'
        ));

        // var_dump($venues->data);
        // die();
        $response = new Response($this->app['twig']->render('agenda/index.twig', [
            'data'      => $this->mapRefactor($venues->data)
        ]));
        //return new JsonResponse(['status' => "success", 'data' => $venues]);
       return $response;
    }

    private function mapRefactor($venues){

        setlocale(LC_ALL,"ca_ES");

        foreach ($venues as $key => $value) {

           // $date = DateTime::createFromFormat("d/m/Y", $value->start_time);

            $event_day = date("d-m-Y", strtotime($value->start_time) );
            //$end_date  = date("d-m-Y", strtotime($value->end_time) );
            $today = date('d-m-Y');
            $datetime1 = new DateTime($event_day);
            $datetime2 = new DateTime($today);
            $eventUNIXtime = strtotime($event_day);
           
            if($eventUNIXtime > strtotime($today)){
                $date = DateTime::createFromFormat("d-m-Y", $event_day);

                $value->start_day       = strtoupper(strftime("%a",$date->getTimestamp()));
                $value->start_day_num   = date("d", strtotime($value->start_time));
                $value->start_month     = strtoupper(strftime("%B",$date->getTimestamp()));
                $value->reacted         = $value->attending_count + $value->interested_count;
                $value->description     = substr($value->description, 0, 250) . '...';
                $sort[$key]             = strtotime($eventUNIXtime);

            }else{
                unset($venues[$key]);
            }
        }

       // var_dump($venues);
        // $venues_result = array_multisort($sort, SORT_DESC, (array) $venues);
        // var_dump($venues_result);
        return $venues;

    }


    public function article( $url )
    {

        // $article = $this->BlogModel->getArticle($url);
        // if($article){

        //     //PATCH GUARRO PER MIGRAR A USER MODEL
        //     if(!$article[0]['avatar']){
        //         $article[0]['avatar'] = 'default_avatar.jpg';
        //     }
        //         $session = true;

        //     if(!@$this->app['session']->get('uid')){
        //         $session = false;
        //     }

        //     if($article[0]['active'] || $session){

        //         $response = new Response($this->app['twig']->render('agenda/index.twig', [
        //             'session'   => $session,
        //             'data'      => $article[0]
        //         ]));

        //          return $response;

        //     }

        //     //AQUI el 404

        //     return false;
        // }

        // //RETORNAR PAGINA 404
        // return false;
    }    

}
