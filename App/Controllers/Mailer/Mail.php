<?php

namespace Controllers\Mailer;

/**
 * Class Mail
 * @package Controllers\Mailer
 */
class Mail
{
    /**
     * @var
     */
    private $subject;

    /**
     * @var
     */
    private $body;

    /**
     * @var
     */
    private $address;

    /**
     * @param $subject
     * @param $body
     * @param $address
     */
    public function __construct($subject, $body, $address)
    {
        $this->subject = $subject;
        $this->body    = $body;
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }
}
