<?php

namespace Controllers\Login;

use Model\Names\NameFilterModel;
use Symfony\Component\HttpFoundation\Response;
use Model\User\UserModel;

/**
 * Class LoginController
 * @package Controllers\Login
 */
class LoginController
{
    /**
     * @var \Silex\Application
     */
    private $app;

    /**
     * @param $app
     */
    public function __construct($app, UserModel $UserModel)
    {
        $this->app = $app;
        $this->UserModel = $UserModel;
    }

    /**
     * @return Response
     */
    public function index( $main_message = false )
    {
        
        $response = new Response($this->app['twig']->render('login.twig', [
            "total_socis"    => $this->UserModel->countUsers(),
            "missatge"       => $main_message,
        ]));

        return $response;
    }
}
